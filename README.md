# Assessment on NextJs #


### Installation Instruction Steps :- ###
Step 1) Open any IDE.  
Step 2) Take clone (git clone https://gitlab.com/riteshm/assessment_full_stack-spec.git).  
Step 3) Go inside the correct folder (cd assessment_full_stack-spec/)  
Step 4) Install Node package dependencies (npm install).    
Step 5) Run Project (npm run dev).     
Step 6) The project will open in the localhost (generaly it opens at http://localhost:3000).     


### Reference :- https://www.figma.com/file/iiYY1S9CkL9pCxGoTr3Xqo/Untitled?type=design&node-id=0-1&mode=design ###


### Project Requirement ###
1. Front-end task: Replicate the LHS vertical box, as closely as possible. They need to be interactive (e.g., checkboxes), but they do not need to “do” anything when clicked (e.g.,
“Action 1” does not lead anywhere).     
2. Back-end task: Create a grid of 2x2 as shown. When each box is clicked, you should make an API call to the following endpoints and go to a new page to display the result.  

a. *Cat fact*  
i. Endpoint https://caeact.ninja/fact  
ii. What to show: the fact in plaintext on a new page  

b. *Bitcoin price*  
i. Endpoint https://api.coindesk.com/v1/bpi/currentprice.json  
ii. What to show: the USD rate (e.g., $30,600)  

c. *Activity*  
i. Endpoint https://www.boredapi.com/api/acMvity  
ii. What to show: the acMvity  

d. *Dog image*  
i. Endpoint https://dog.ceo/api/breeds/image/random  
ii. What to show: the image, embedded in the page  

Tech stack to use  
• Next.js for the application  
• Anything else as necessary  


### Project UI ###
![alt text](https://gitlab.com/riteshm/assessment_full_stack-spec/-/raw/main/public/Screenshot%20from%202023-06-29%2017-54-32.png)


### Please visit - https://assessment-full-stack-spec.vercel.app/
   
   
## Feedback

 - Any suggestions or feedback to improve it, is most welcomed.
If you have any feedback & queries, please reach out to us at ritesh.m@inhertix.com



### Next version :- 13.4 ###
