import Link from "next/link";
import "./page.css";

export default function Home() {
  const listItems = [
    { id: 1, type: "A", title: "List item", rate: "100+" },
    { id: 2, type: "A", title: "List item", rate: "100+" },
    { id: 3, type: "A", title: "List item", rate: "100+" },
  ];

  const gridBoxes = [
    {
      id: 1,
      title: "Cat fact",
      route: "/Cat_Fact",
      endpoint: "https://catfact.ninja/fact",
    },
    {
      id: 2,
      title: "Bitcoin price",
      route: "/Bitcoin_price",
      endpoint: "https://api.coindesk.com/v1/bpi/currentprice.json",
    },
    {
      id: 3,
      title: "Activity",
      route: "/Activity",
      endpoint: "https://www.boredapi.com/api/activity",
    },
    {
      id: 4,
      title: "Dog image",
      route: "/Dog",
      endpoint: "https://dog.ceo/api/breeds/image/random",
    },  
  ];

  return (
    <main className="main">
      <div className="main-container">
        <div className="first-container">
          <div className="first-container-upper-section">
            <h2>Upwork</h2>
            <div>
              Upwork connects businesses with independent professionals and
              agencies around the globe. Where companies and freelancers work
              together in new ways that...
            </div>
          </div>

          <div className="first-container-middle-section">
            {listItems.map((elem) => {
              return (
                <li
                  className="first-container-middle-section-list"
                  key={elem.id}
                >
                  <div className="first-container-middle-section-list-first-div">
                    <div className="first-container-middle-section-list-first-div-avatar">
                      {elem.type}
                    </div>
                    <div className="first-container-middle-section-list-first-div-title">
                      {elem.title}
                    </div>
                  </div>
                  <div className="first-container-middle-section-list-second-div">
                    <span className="first-container-middle-section-list-second-div-rate">
                      {elem.rate}
                    </span>{" "}
                    <span>
                      <input type="checkbox" />
                    </span>
                  </div>
                </li>
              );
            })}
          </div>
          <div className="first-container-lower-section">
            <span>Action 1</span> <span>Action 2</span>
          </div>
        </div>

        <div className="second-container">
          <div className="second-container-main-div">
            {gridBoxes.map((elem) => {
              return (
                <Link href={elem.route}>
                  <div
                    className="second-container-main-div-units"
                    key={elem.id}
                  >
                    {elem.title}
                  </div>
                </Link>
              );
            })}
          </div>
        </div>
      </div>
    </main>
  );
}
