"use client";
import Link from "next/link";
import { useEffect, useState } from "react";

const Cat_Fact = () => {
  const [catfact, setCatfact] = useState();
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    setLoader(true);
    const fetchData = async () => {
      try {
        const response = await fetch("https://catfact.ninja/fact");
        const data = await response.json();
        setCatfact(data?.fact);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
    setLoader(false);
  }, []);

  return (
    <div className="api-content">
      {loader && <div>...Loading</div>}
      {catfact}
      <Link href="/">
        <button>Go Back to Home</button>
      </Link>
    </div>
  );
};

export default Cat_Fact;
