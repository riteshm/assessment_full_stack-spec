"use client";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";

const Dog = () => {
  const [dogImage, setDogImage] = useState();
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    setLoader(true);
    const fetchData = async () => {
      try {
        const response = await fetch("https://dog.ceo/api/breeds/image/random");
        const data = await response.json();
        setDogImage(data.message);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
    setLoader(false);
  }, []);

  return (
    <>
      <div className="api-content">
        {loader && <div>...Loading</div>}
        <Image src={dogImage} alt="dog" width={400} height={400} />
        <Link href="/">
          <button>Go Back to Home</button>
        </Link>
      </div>
    </>
  );
};

export default Dog;
