"use client";
import Link from "next/link";
import { useEffect, useState } from "react";

const Bitcoin_price = () => {
  const [USDrate, setUSDrate] = useState();
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    setLoader(true);
    const fetchData = async () => {
      try {
        const response = await fetch(
          "https://api.coindesk.com/v1/bpi/currentprice.json"
        );
        const data = await response.json();
        setUSDrate(data?.bpi?.USD?.rate);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
    setLoader(false);
  }, []);

  return (
    <div className="api-content">
      {loader && <div>...Loading</div>}
      {USDrate}{" "}
      <Link href="/">
        <button>Go Back to Home</button>
      </Link>
    </div>
  );
};

export default Bitcoin_price;
