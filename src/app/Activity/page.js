"use client";
import Link from "next/link";
import { useEffect, useState } from "react";

const Activity = () => {
  const [activity, setActivity] = useState();
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    setLoader(true);
    const fetchData = async () => {
      try {
        const response = await fetch("https://www.boredapi.com/api/activity");
        const data = await response.json();
        setActivity(data?.activity);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
    setLoader(false);
  }, []);

  return (
    <div className="api-content">
      {loader && <div>...Loading</div>}
      {activity}
      <Link href="/">
        <button>Go Back to Home</button>
      </Link>
    </div>
  );
};

export default Activity;
